from django.contrib import admin
from .models import Sticker, Comment, Asset, License, Event


# Register your models here.
admin.site.register(Sticker)
admin.site.register(Comment)
admin.site.register(Asset)
admin.site.register(License)
admin.site.register(Event)
