from django.db import models
from django.contrib.auth.models import User


class License(models.Model):
    license = models.CharField(max_length=120, null=False, blank=False)
    description = models.TextField()

    def __str__(self):
        return self.license


class Event(models.Model):
    event = models.TextField(max_length=100)


class Sticker(models.Model):
    name = models.CharField('sticker name', max_length=100, blank=False, null=False)
    description = models.TextField('description', blank=True, null=True)
    preview = models.ImageField(upload_to='uploads/%Y/%m/', blank=True, null=True)
    license = models.ForeignKey(License, on_delete=models.SET_NULL, blank=False, null=True)
    at_events = models.ManyToManyField(Event, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    created_at = models.DateTimeField('created at', auto_now_add=True, blank=True, null=False)

    def __str__(self):
        return self.name


class Batch(models.Model):
    sticker = models.ForeignKey(Sticker, on_delete=models.CASCADE, blank=False, null=False)
    copies = models.PositiveIntegerField(default=1000, blank=False, null=False)
    printed_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    printed_at = models.DateField(null=True, blank=True)
    sticker_size = models.CharField(max_length=30)
    sticker_material = models.CharField(max_length=100)

    def __str__(self):
        return '{} ({}) - {}'.format(self.sticker.name, self.copies, self.printed_at)


class Comment(models.Model):
    comment = models.TextField('comment', blank=False, null=False)
    sticker = models.ForeignKey(Sticker, on_delete=models.CASCADE, null=False, blank=False, related_name='comment_sticker')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False, related_name='comment_by')
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    last_modified_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    modified_count = models.PositiveSmallIntegerField(default=0, null=False)

    def __str__(self):
        return 'comment for "{}" by "{}" written at "{}": {}...'.format(self.sticker.name, self.created_by.username, self.created_at, self.comment[0:50])


class Asset(models.Model):
    file = models.FileField(upload_to='uploads/%Y/%m/')
    sticker = models.ForeignKey(Sticker, on_delete=models.CASCADE, null=False, blank=False, related_name='asset_sticker')
    description = models.CharField(max_length=140, null=True, blank=True)
    uploaded_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False, related_name='asset_uploaded_by')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    version = models.PositiveSmallIntegerField(default=0, null=False)
    mimetype = models.CharField(max_length=100, default='unknown', null=False, blank=True)

    def __str__(self):
        return 'Asset "{}" for "{}" in version "{}"'.format(self.file, self.sticker.name, self.version)
