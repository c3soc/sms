from django.shortcuts import render, HttpResponse
from .models import Sticker, Comment


# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the sms index.")


def sticker_list(request):
    stickers = Sticker.objects.order_by('-created_at').prefetch_related().all()
    context = {'sticker_list': stickers}
    return render(request, 'sms/stickers.html', context)


def sticker(request, sid):
    single_sticker = Sticker.objects.get(id=sid)
    context = {'sticker': single_sticker}
    return render(request, 'sms/sticker.html', context)