from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('sticker/', views.sticker_list, name='sticker_list'),
    path('sticker/<int:sid>', views.sticker, name='sticker_details')
]
